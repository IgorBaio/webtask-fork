/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.controller.service;

import br.com.webtask.aula.domain.model.Task;
import br.com.webtask.aula.domain.repo.TaskRepo;
import java.time.LocalDate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class TaskServiceTest {

    @InjectMocks
    private TaskService task;

    @MockBean
    private TaskRepo taskRepo;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testMethodFinalizer() throws Exception {
        //cenario
        Task taskOne = new Task(1112223334445556662l, "Dormir", LocalDate.now().minusDays(2), null, null);
        Task taskTwo = new Task(1122223334445556662l, "Ler", LocalDate.now().minusDays(2), LocalDate.now(), null);
        Mockito.when(taskRepo.save(Mockito.any(Task.class))).thenReturn(taskTwo);
        //execução
        Task t = task.finalizar(taskOne);
        //verificação
        Assertions.assertTrue(t.isFinish());
    }

    @Test
    public void testSecondMethodFinalizer() {
        //cenario
        Task taskOne = new Task(1112223334445556662l, "", LocalDate.now().minusDays(2), null, null);
        Task taskTwo = new Task(1122223334445556662l, "", LocalDate.now().minusDays(2), LocalDate.now(), null);
        Mockito.when(taskRepo.save(Mockito.any(Task.class))).thenReturn(taskTwo);
        //execução
        Task t;
        
        try {
            t = task.finalizar(taskOne);
            Assertions.fail("deveria gerar um erro");
        } catch (Exception ex) {
            //verificação
            Assertions.assertTrue(true);
        }
    }

}
