///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package br.com.webtask.aula.selenium;
//
//import java.time.Duration;
//import org.assertj.core.api.AssertionsForClassTypes;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.openqa.selenium.By;
//import org.openqa.selenium.Dimension;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.web.server.LocalServerPort;
//import org.springframework.test.context.ActiveProfiles;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.support.ui.WebDriverWait;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@ActiveProfiles("test")
//public class FunctionScreenTest {
//    
//    @LocalServerPort
//    private int port;
//    
//    private WebDriver driver = new ChromeDriver();;
//    
//    @BeforeEach
//    public void setUp(){
//        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
//    }
//    
//    @AfterEach
//    public void tearDown(){
//        driver.quit();
//    }
//    
//    @Test
//    public void testSaveTask() {
//        driver.get("http://localhost:"+port+"/login");
//        driver.manage().window().setSize(new Dimension(1352, 616));
//        //driver.findElement(By.id("username")).click();
//        driver.findElement(By.id("username")).sendKeys("123");
//        //driver.findElement(By.id("password")).click();
//        driver.findElement(By.id("password")).sendKeys("123");
//        driver.findElement(By.cssSelector(".login100-form-btn")).click();
//        driver.findElement(By.cssSelector(".nav-item:nth-child(4) span")).click();
//        //driver.findElement(By.id("cpNome")).click();
//        driver.findElement(By.id("cpNome")).sendKeys("tarefa para se testar os testes");
//        //driver.findElement(By.id("cpData")).click();
//        driver.findElement(By.id("cpData")).sendKeys("2020-12-22");
//        driver.findElement(By.cssSelector(".btn-primary")).click();
//        driver.findElement(By.cssSelector(".nav-item:nth-child(5) span")).click();
//        
//        AssertionsForClassTypes.assertThat(driver.findElement(By.cssSelector("tr:nth-child(2) > td:nth-child(1)")).getText()).isEqualTo("tarefa para se testar os testes");
//        
//    }
//    
//}
