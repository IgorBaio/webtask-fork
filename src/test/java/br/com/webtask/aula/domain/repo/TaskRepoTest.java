/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.domain.repo;

import br.com.webtask.aula.domain.model.Task;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DataJpaTest
public class TaskRepoTest {
    
    @Autowired
    private TaskRepo task;
    
    Task taskOne, taskTwo;
    
    @BeforeEach
    public void init(){
        taskOne = new Task(1112223334445556662l,"Estudar",LocalDate.now().minusDays(2),null,null);
        taskTwo = new Task(1122223334445556662l,"Ler",LocalDate.now().plusDays(2),null,null);
        
        task.save(taskOne);
        task.save(taskTwo);
    }
    
    @AfterEach
    public void afterInit(){
        task.deleteAll();
    }

    @Test
    public void testTaskDescription() {
        //cenario
        
        //execução
        List<Task> listTasks = task.findByTaskDescription("Estudar");
        
        //verificação
        Assertions.assertEquals(1, listTasks.size());
    }
    
    @Test
    public void testTaskSecondDescription() {
        //cenario
        
        //execução
        List<Task> listTasks = task.findByTaskDescription("Ler");
        Task task = listTasks.get(0);
        
        //verificação
        Assertions.assertEquals("Ler", task.getTaskDescription());
    }
    
}
