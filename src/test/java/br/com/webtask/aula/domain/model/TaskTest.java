/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.webtask.aula.domain.model;

import java.time.LocalDate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;


public class TaskTest {
    
    @Test
    @DisplayName("Verificando se uma task atrasada, fica com status de atrasada.")
    public void testeStatusTaskAtrasada(){
        //cenario
        Task task = new Task(1112223334445556662l,"Estudar",LocalDate.now().minusDays(2),null,null);
        //executar
        EStatus status = task.getStatus();
        //verificar
        Assertions.assertEquals(EStatus.ATRASADO, status);
    }
    
}
